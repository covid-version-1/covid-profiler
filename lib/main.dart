import 'package:covid_app/profile.dart';
import 'package:covid_app/question.dart';
import 'package:covid_app/risk.dart';
import 'package:covid_app/vaccine.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: 'Covid 19',
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String name = "";
  String gender = "M";
  int age = 0;
  var questionScore = 0.0;
  var riskScore = 0.0;

  var vaccines = ['-', '-', '-'];

  @override
  void initState() {
    super.initState();
    _loadProfile();
    _loadQuestion();
    _loadRisk();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      vaccines = prefs.getStringList('vaccines') ?? ['-', '-', '-'];
    });
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('gander');
    prefs.remove('age');
    prefs.remove('vaccines');
    prefs.remove('question_value');
    prefs.remove('risk_value');
    await _loadProfile();
    await _loadQuestion();
    await _loadRisk();
  }

  Future<void> _loadQuestion() async {
    var perfs = await SharedPreferences.getInstance();
    var strQuestionValue = perfs.getString('question_value') ??
        '[false,false,false,false,false,false,false,false,false,false,false]';
    var arrayStr =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      questionScore = 0.0;
      for (var i = 0; i < arrayStr.length; i++) {
        if (arrayStr[i].trim() == 'true') {
          questionScore = questionScore + 1;
        }
      }
      questionScore = (100 * questionScore) / 11;
    });
  }

  Future<void> _loadRisk() async {
    var perfs = await SharedPreferences.getInstance();
    var riskStrQuestionValue = perfs.getString('risk_value') ??
        '[false,false,false,false,false,false,false]';
    var riskArrayStr = riskStrQuestionValue
        .substring(1, riskStrQuestionValue.length - 1)
        .split(',');
    setState(() {
      riskScore = 0.0;
      for (var i = 0; i < riskArrayStr.length; i++) {
        if (riskArrayStr[i].trim() == 'true') {
          riskScore = riskScore + 1;
        }
      }
      riskScore = (100 * riskScore) / 7;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Main')),
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text(name),
                  subtitle: Text(
                    'เพศ $gender, อายุ: $age ปี',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'อาการ : ${questionScore.toStringAsFixed(2)}%',
                    style: TextStyle(
                        fontWeight: (questionScore > 30)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 26.0,
                        color: (questionScore > 30)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'ความเสี่ยง : ${riskScore.toStringAsFixed(2)}%',
                    style: TextStyle(
                        fontWeight: (riskScore > 20)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 26.0,
                        color: (riskScore > 12)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.black.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'ได้รับวัคซีน : ${vaccines.toString()}',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.start,
                  children: [
                    TextButton(
                      onPressed: () async {
                        await _resetProfile();
                      },
                      child: const Text('Reset'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            title: Text('Profile'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfileWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
            title: Text('Vaccine'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => VaccineWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
            title: Text('Question'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => QuestionWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
            title: Text('Risk'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RiskWidget()));
              await _loadProfile();
            },
          ),
        ],
      ),
    );
  }
}
