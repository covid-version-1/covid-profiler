import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QuestionWidget extends StatefulWidget {
  QuestionWidget({Key? key}) : super(key: key);

  @override
  _QuestionWidgetState createState() => _QuestionWidgetState();
}

class _QuestionWidgetState extends State<QuestionWidget> {
  var questionValue = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var questions = [
    'มีไข้',
    'ไอ',
    'แน่นหน้าอก',
    'เหนื่อยล้า',
    'ปวดกล้ามเนื้อ',
    'ปวดหัว',
    'ไม่ได้กลิ่นและรส',
    'เจ็บคอ',
    'คัดจมูกน้ำมูกไหล',
    'คลื่นไส้อาเจียน',
    'ท้องเสีย',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Question')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: questionValue[0],
              title: Text(questions[0]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[1],
              title: Text(questions[1]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[2],
              title: Text(questions[2]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[3],
              title: Text(questions[3]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[4],
              title: Text(questions[4]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[5],
              title: Text(questions[5]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[6],
              title: Text(questions[6]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[7],
              title: Text(questions[7]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[8],
              title: Text(questions[8]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[8] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[9],
              title: Text(questions[9]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[9] = newValue!;
                });
              }),
          CheckboxListTile(
              value: questionValue[10],
              title: Text(questions[10]),
              onChanged: (newValue) {
                setState(() {
                  questionValue[10] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                {
                  await _saveQuestion();
                  Navigator.pop(context);
                }
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var perfs = await SharedPreferences.getInstance();
    var strQuestionValue = perfs.getString('question_value') ??
        '[false,false,false,false,false,false,false,false,false,false,false]';
    var arrayStr =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrayStr.length; i++) {
        questionValue[i] = (arrayStr[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var perfs = await SharedPreferences.getInstance();
    perfs.setString('question_value', questionValue.toString());
  }
}
